# Source Files

The files for immediate use are found in the `dist` folder. Print the
PDFs, or use the EPUB files for ebook readers.


## Scribus

The `.sla` files are [scribus](http://scribus.net) source files for the
free and open source page layout application.

Open them in Scribus and modify them to your taste, or else export
them to PDF and print.

## Docbook

The epub files are generated from docbook XML. To generate EPUB from
these files:

1. create and set a DOCBOOK environment variable. You can do this on an as-needed basis:

       $ DOCBOOK=/path/to/docbookx.dtd make epub

   Or you can set it in your `.bashrc` or `.bash_profile`

       $ echo "DOCBOOK=/usr/share/xml/docbook/schema/dtd/\
         4.5/docbookx.dtd" >> $HOME/.bash_profile
       $ echo "export DOCBOOK" >> $HOME/.bash_profile
       $ source $HOME/.bash_profile

2. install [docbook](http://docbook.org)

       $ sudo blah install docbook

3. install a converter like [xmlto](https://fedorahosted.org/xmlto/) or [fop](https://xmlgraphics.apache.org/fop/)

       $ sudo blah install xmlto


### Building with Docbook

Xmlto is easiest:

    $ make epub

Which produces the file `d6.epub`, `d10.epub`, and `shift.epub`, all located in the `dist` directory.


