# Pocket Dice Roller

Playing a game, but don't have dice handy?

**Pocket Dice Roller** is a "pocketmod" booklet to produce pseudo dice
rolls. It folds into a small wallet-size 8-page reversible
booklet.

Here's how it works, using a standard **d6** (6 sided die) roll as an
example:

## First Roll Only

For your first roll, or any time you need to re-seed your roll:

1. Choose a letter from **A** to **F**

2. Flip the book a few times and decide which is the "front", and
   which direction you want to turn pages (left-to-right or
   right-to-left are both valid). The booklet can be flipped and
   flopped to increase entropy.

3. Consult one of the **d6** charts (the ones with 6 entries, not the
   gray ones with 10) on the front of the book to find the letter you
   picked. Turn to the page number indicated next to your letter
   choice.

4. On this page, your roll is the number next to your letter. This
   number also serves as the page to turn to on your next roll.


## All Subsequent Rolls

1. Choose a letter from **A** to **F**

2. Rotate the book a few times to decide which is the "front" and
   "right-side up"

3. Turn to the page number you got as your *previous* roll (for example,
   if you rolled a 6 on your previous turn, turn to page 6)

4. On the page you turn to, consult the chart. Your roll is the number
   indicated next to the letter you chose. It is also the page you'll
   use on your next roll. If it's the same number as the page you're
   on, flip or flop the book on your next roll.


## Alternate Rolls

* For d10, use the gray box.

* For d12, roll d6 twice. 

* For d20, roll d10 twice.

There are only 8 pages in the **Pocket Dice Roller** booklet, so for
page numbers greater than 8, either loop around from the start again,
or co unt charts instead of pages (there are 16 charts total, so for
d20 you'll still have to loop around from the start).

Alternately, devise your own system to produce a seed (for example:
use the roll result minus the number of rows from the top of the chart
to your roll result). As long as you cannot easily predict what page
you'll be using as your key prior to picking your letter, the system
remains unpreditable, and therefore "random enough" for most games.


## Notes

There are intentionally no page numbers so that you can flip and flop
the booklet as needed. You should choose to turn pages left-to-right
some of the time, and right-to-left other times.

Your greatest enemy is predictability, so as long as you can't predict
what chart on page 3 you'll be referencing, or even what page is page
3 in the first place, you will get results that feel random.

If you miscount or skip a page by accident, don't correct yourself!
The more entropy, the better.



## Digital Editions

The digital edition is meant for use on ebook readers. It uses a
similar principle as the paper edition.

When you open the digital roller, choose a letter to start with. This
is your seed number, and takes you to a page of rollable letters.

Choose a letter for your result.

Click or touch your result to re-seed, and now you may roll again.

If you feel you're getting stuck in a loop, re-seed.

There are separate files for d6 and d10 rolls.


## Know Issues

This system is not mathematically random, and its probability is not
the same as dice probability. This method generates "unexpected"
numbers, not random.

This system is non-admissible into most casinos or in professional
Poker games. Also, I don't think they use dice in Poker.

This system should not be used for encryption any more than it already
has been.

## Bugs

Report bugs on gitlab.com/notklaatu/pocketdiceroller

Improvements are welcome.